package de.gartenbauvereineschwaben.tools;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;

public class LocationGarten
{
    @JsonProperty("n")
    private String name;
    @JsonProperty("s")
    private String strasse;
    @JsonProperty("k")
    private String kreis;
    @JsonProperty("o")
    private String ort;
    @JsonProperty("ot")
    private String ortsteil;
    @JsonProperty("t")
    private String text = "";

    @JsonProperty("gps")
    private Place gps = new Place();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStrasse() {
        return strasse;
    }

    public void setStrasse(String strasse) {
        this.strasse = strasse;
    }

    public String getKreis() {
        return kreis;
    }

    public void setKreis(String kreis) {
        this.kreis = kreis;
    }

    public String getOrt() {
        return ort;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }

    public String getOrtsteil() {
        return ortsteil;
    }

    public LocationGarten setOrtsteil(String ortsteil) {
        this.ortsteil = ortsteil;
        return this;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Place getGps() {
        return gps;
    }

    public static class Place {
        double lt;
        double lng;

        public double getLt() {
            return lt;
        }

        public void setLt(double lt) {
            this.lt = lt;
        }

        @JsonGetter("long")
        public double getLng() {
            return lng;
        }

        public void setLng(double lng) {
            this.lng = lng;
        }
    }


}
