package de.gartenbauvereineschwaben.tools;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.text.CollationKey;
import java.text.Collator;
import java.util.Locale;
import java.util.Objects;

public class StreuobstSorte  {

    private static Collator germanCollator = Collator.getInstance(Locale.GERMAN);
    static {
        germanCollator.setStrength(Collator.TERTIARY);
    }


    private String sortenNr;

    private String sortenName;
    private String art;
    private String linkId;

    public StreuobstSorte() {
    }
    public StreuobstSorte(LocationObst location) {

        this.sortenName = location.getSorte();
        this.art = location.getArt();
    }

    public StreuobstSorte(String sortenName) {
        this.sortenName = sortenName;
    }

    @JsonGetter("nr")
    public String getSortenNr() {
        return sortenNr;
    }

    @JsonSetter("nr")
    public StreuobstSorte setSortenNr(String sortenNr) {
        this.sortenNr = sortenNr;
        return this;
    }

    @JsonGetter("n")
    public String getSortenName() {
        return sortenName;
    }

    @JsonSetter("n")
    public StreuobstSorte setSortenName(String sortenName) {
        this.sortenName = sortenName;
        return this;
    }

    @JsonIgnore
    public String getSorteKonsolidiert() {

        return sortenName;
    }

    @JsonIgnore
    public CollationKey getSortenNameCollation() {

        return germanCollator.getCollationKey(getSortenName());
    }


    @JsonGetter("a")
    public String getArt() {
        return art;
    }

    @JsonSetter("a")
    public StreuobstSorte setArt(String art) {
        this.art = art;
        return this;
    }

    @JsonGetter("id")
    public String getLinkId() {
        return linkId;
    }

    @JsonSetter("id")
    public StreuobstSorte setLinkId(String linkId) {
        this.linkId = linkId;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StreuobstSorte that = (StreuobstSorte) o;
        return Objects.equals(sortenName, that.sortenName) && Objects.equals(art, that.art);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sortenName, art);
    }

}
