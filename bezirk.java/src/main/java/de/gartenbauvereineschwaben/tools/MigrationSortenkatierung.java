package de.gartenbauvereineschwaben.tools;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class MigrationSortenkatierung {




    private static final double NEU_ULM_WEST = 9.9674;
    private static final double NEU_ULM_EAST = 10.3153;
    private static final double NEU_ULM_NORTH = 48.4776;
    private static final double NEU_ULM_SOUTH = 48.1083;

    public static void main(String[] args) throws IOException, CsvException {

        //create ObjectMapper instance
        ObjectMapper objectMapper = new ObjectMapper();

        Reader filereader = new FileReader("C:\\Users\\rschneider\\Documents\\rainer\\ogv\\streuobsst\\Bäume_Nordschwaben_26.01.22.CSV");
        CSVParser parser = new CSVParserBuilder().withSeparator(';').build();
        CSVReader csvReader = new CSVReaderBuilder(filereader).withCSVParser(parser).build();
        List<String[]> allData = csvReader.readAll();
        allData.remove(0); // remove header
        List<LocationObst> locations = allData.stream()
                .map(feature -> mapToLocationObst(feature))
                .collect(Collectors.toList());

        //read json file and convert to customer object
        FeatureCollection features = objectMapper.readValue(new File("C:\\Project\\rainer\\ogveditor\\app.backend\\src\\main\\resources\\migration\\SortenkartierungSchwaben.json"), FeatureCollection.class);


        //print customer details
 //       System.out.println(locations);
        File targetFile = new File("C:\\Project\\rainer\\ogveditor\\app.backend\\src\\main\\resources\\migration\\StreuObst2.json");
        objectMapper.writeValue(targetFile, locations);
    }

    private static LocationObst mapToLocationObst(String[] field) {

        LocationObst location = new LocationObst();
        location.setNr(field[0]);
        location.setSorte(field[2]);
        LocationObst.Place place = new LocationObst.Place();
        place.setLt(Double.parseDouble(field[11].replace(',', '.')));
        place.setLng(Double.parseDouble(field[12].replace(',', '.')));
        location.setOrt(place);
        location.setKreis(field[22]);
        location.setGemeinde(field[23]);
        location.setArt(field[29]);
        return location;
    }



    private static LocationInMap mapToLocationInMap(Feature feature) {

        LocationInMap location = new LocationInMap();
        location.setId(feature.getId());
        location.setName(locationAddress(feature));
        location.setBeschreibung(feature.getProperties().getName() + "," + feature.getProperties().getContent());
        location.setRange("NU");
        LocationInMap.Place place = new LocationInMap.Place();
        place.setLat(feature.getGeometry().getCoordinates()[1]);
        place.setLongitude(feature.getGeometry().getCoordinates()[0]);
        location.setPlace(place);
        location.setOrt("");
        location.setStrasse("");
        location.setUrl("");

        return location;
    }

    private static String locationAddress(Feature feature) {
        String desc = feature.getProperties().getDescription();
        Document doc = Jsoup.parse(desc);
        Elements paragraphs = doc.select("span.locationaddress");
        if(paragraphs.size() > 0) {
            "Baum Nr.".length();
            String name =  paragraphs.get(0).text();
            int end = name.indexOf(' ', "Baum Nr. ".length());
            return name.substring(0, end);
        } else {
            return "";
        }

    }

    private static Predicate<? super Feature> isInLandkreisNeuUlm() {

        return feature -> {
            double longitude = feature.getGeometry().getCoordinates()[0];
            double lat = feature.getGeometry().getCoordinates()[1];

            return longitude > NEU_ULM_WEST && longitude < NEU_ULM_EAST
                    && lat > NEU_ULM_SOUTH && lat < NEU_ULM_NORTH;
        };
    }

    public static class FeatureCollection
    {
        private String type;
        private Feature[] features;
        private String pagination;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public Feature[] getFeatures() {
            return features;
        }

        public void setFeatures(Feature[] features) {
            this.features = features;
        }

        public String getPagination() {
            return pagination;
        }

        public void setPagination(String pagination) {
            this.pagination = pagination;
        }
    }

    public static class Feature
    {
        private String id;
        private String type;
        private FeatureGeometry geometry;
        private FeaturePoperties properties;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public FeaturePoperties getProperties() {
            return properties;
        }

        public void setProperties(FeaturePoperties properties) {
            this.properties = properties;
        }

        public FeatureGeometry getGeometry() {
            return geometry;
        }

        public void setGeometry(FeatureGeometry geometry) {
            this.geometry = geometry;
        }
    }

    public static class FeatureGeometry
    {
        private String type;
        private double[] coordinates;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public double[] getCoordinates() {
            return coordinates;
        }

        public void setCoordinates(double[] coordinates) {
            this.coordinates = coordinates;
        }
    }


    public static class FeaturePoperties
    {
        private String name;
        private String url;
        private String description;
        private String rating;
        private String fulladdress;
        private String content;
        private String icon;
        private Integer itemid;
        private String featured;
        private String distance;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }

        public String getFulladdress() {
            return fulladdress;
        }

        public void setFulladdress(String fulladdress) {
            this.fulladdress = fulladdress;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }

        public void setItemid(Integer itemid) {
            this.itemid = itemid;
        }

        public String getFeatured() {
            return featured;
        }

        public void setFeatured(String featured) {
            this.featured = featured;
        }

        public String getDistance() {
            return distance;
        }

        public void setDistance(String distance) {
            this.distance = distance;
        }
    }

}
