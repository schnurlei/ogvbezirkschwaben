package de.gartenbauvereineschwaben.tools;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.util.HashMap;
import java.util.Map;

public class LocationObst
{
    @JsonProperty("n")
    private String nr;
    @JsonProperty("s")
    private String sorte;
    @JsonProperty("o")
    private Place ort = new Place();
    @JsonProperty("k")
    private String kreis;
    @JsonProperty("g")
    private String gemeinde;
    @JsonProperty("a")
    private String art = "";

    @JsonGetter("n")
    public String getNr() {
        return nr;
    }

    @JsonSetter("n")
    public void setNr(String nr) {
        this.nr = nr;
    }

    @JsonGetter("s")
    public String getSorte() {
        return sorte;
    }

    @JsonSetter("s")
    public void setSorte(String sorte) {
        this.sorte = sorte;
    }
    @JsonGetter("o")
    public Place getOrt() {
        return ort;
    }
    @JsonSetter("o")
    public void setOrt(Place ort) {
        this.ort = ort;
    }
    @JsonGetter("k")
    public String getKreis() {
        return kreis;
    }
    @JsonSetter("k")
    public void setKreis(String kreis) {
        this.kreis = kreis;
    }

    @JsonGetter("g")
    public String getGemeinde() {
        return gemeinde;
    }
    @JsonSetter("g")
    public void setGemeinde(String gemeinde) {
        this.gemeinde = gemeinde;
    }
    @JsonGetter("a")
    public String getArt() {
        return art;
    }
    @JsonSetter("a")
    public void setArt(String art) {
        this.art = art;
    }



    public static class Place {
        double lt;
        double lng;

        public Place() {
        }

        public Place(double lt, double lng) {
            this.lt = lt;
            this.lng = lng;
        }

        public double getLt() {
            return lt;
        }

        public void setLt(double lt) {
            this.lt = lt;
        }

        @JsonGetter("long")
        public double getLng() {
            return lng;
        }

        public void setLng(double lng) {
            this.lng = lng;
        }
    }

    public static final Map<String, String> SORTEN_KONSOLIDIERT = sortenKonsoldierung();

    private static Map<String, String> sortenKonsoldierung() {


        Map<String, String> map = new HashMap<>();


        
        return map;
    }
}
