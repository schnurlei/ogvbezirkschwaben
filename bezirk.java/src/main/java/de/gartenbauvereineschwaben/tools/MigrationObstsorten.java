package de.gartenbauvereineschwaben.tools;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class MigrationObstsorten {

    private static final Map<String,String> art2Art = new HashMap<>();
    {
        art2Art.put("Apfel", "Apfel");
        art2Art.put("A_ubk", "Apfel");
        art2Art.put("A_AT", "Apfel");
        art2Art.put("Birne", "Birne");
        art2Art.put("B_ubk", "Birne");
        art2Art.put("B_AT", "Birne");
        art2Art.put("Beerenobst", "Beerenobst");
        art2Art.put("Kirsche", "Kirsche");
        art2Art.put("Mispel", "Mispel");
        art2Art.put("Sorbus", "Sorbus");
        art2Art.put("Walnuss", "Walnuss");
        art2Art.put("Zwetschge", "Zwetschge");
    }

    enum USED_COLUMNS {
        NR {
            @Override
            public void setValueInLocation(Row row, Map<USED_COLUMNS, Integer> colName2ColNr, LocationObst location) {
                int colNr = colName2ColNr.get(this);
                location.setNr("" + ((int)row.getCell(colNr).getNumericCellValue()));
            }
        }, SORTE {
            @Override
            public void setValueInLocation(Row row, Map<USED_COLUMNS, Integer> colName2ColNr, LocationObst location) {
                int colNr = colName2ColNr.get(this);
                location.setSorte(row.getCell(colNr).getStringCellValue());
            }
        }, LANDKREIS {
            @Override
            public void setValueInLocation(Row row, Map<USED_COLUMNS, Integer> colName2ColNr, LocationObst location) {
                int colNr = colName2ColNr.get(this);
                location.setKreis(row.getCell(colNr).getStringCellValue());
            }
//        }, GEMEINDE {
//            @Override
//            public void setValueInLocation(Row row, Map<USED_COLUMNS, Integer> colName2ColNr, LocationObst location) {
//                int colNr = colName2ColNr.get(this);
//                location.setGemeinde(row.getCell(colNr).getStringCellValue());
//            }
        }, ART {
            @Override
            public void setValueInLocation(Row row, Map<USED_COLUMNS, Integer> colName2ColNr, LocationObst location) {
                int colNr = colName2ColNr.get(this);
                String art = row.getCell(colNr).getStringCellValue();
                String mappedArt = art2Art.get(art);
                if(mappedArt == null) {
                    throw new RuntimeException("Unbekannte art " + art);
                }
                location.setArt(mappedArt);
            }
        }, PLZ_ORT {
            @Override
            public void setValueInLocation(Row row, Map<USED_COLUMNS, Integer> colName2ColNr, LocationObst location) {
                int colNr = colName2ColNr.get(this);
                if (row.getCell(colNr) != null) {
                    location.setGemeinde(row.getCell(colNr).getStringCellValue());
                }
            }
        }, SORTKURZEL {
            @Override
            public void setValueInLocation(Row row, Map<USED_COLUMNS, Integer> colName2ColNr, LocationObst location) {
                //location.setArt(value);
            }
        }, LONG {
            @Override
            public void setValueInLocation(Row row, Map<USED_COLUMNS, Integer> colName2ColNr, LocationObst location) {
                int colNr = colName2ColNr.get(this);
                location.getOrt().setLng((row.getCell(colNr).getNumericCellValue()));
            }
        }, LAT {
            @Override
            public void setValueInLocation(Row row, Map<USED_COLUMNS, Integer> colName2ColNr, LocationObst location) {
                int colNr = colName2ColNr.get(this);
                location.getOrt().setLt((row.getCell(colNr).getNumericCellValue()));
            }
        };

        public abstract void setValueInLocation(Row row, Map<USED_COLUMNS, Integer> colName2ColNr, LocationObst location );
    }

    public static void main(String[] args) throws IOException {

        Sheet sheet = new MigrationObstsorten().readExcel("C:\\Users\\rschneider\\Documents\\rainer\\ogv\\streuobsst\\geodaten\\Sorten_Allgäu.xlsx");

        List<LocationObst> obstLocations = new MigrationObstsorten().convertCsvLinesToLocations(sheet);


        //print customer details
 //       System.out.println(locations);
        File targetFile = new File("C:\\Project\\rainer\\ogveditor\\app.backend\\src\\main\\resources\\migration\\StreuObst4.json");
        //create ObjectMapper instance
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writeValue(targetFile, obstLocations);
    }

    private Sheet readExcel(String fileName) throws IOException {

        try (InputStream inp = new FileInputStream(fileName)) {

           Workbook workbook = new XSSFWorkbook(inp);

            return workbook.getSheetAt(0);
        }
    }

    private List<LocationObst> convertCsvLinesToLocations( Sheet sheet) {

        Map<USED_COLUMNS, Integer> colName2ColNr = colName2ColNr(sheet.getRow(0));

        List<LocationObst> locations = new ArrayList<>();
        for (Row row : sheet) {
            if(row.getRowNum() != 0) { // ignore header
                locations.add(convertCsvLineToLocations(row,colName2ColNr ));
            }
        }

        return locations;
    }



    private static LocationObst convertCsvLineToLocations(Row row, Map<USED_COLUMNS, Integer> colName2ColNr) {

        LocationObst location = new LocationObst();

        colName2ColNr.forEach((key, value) -> key.setValueInLocation(row, colName2ColNr, location));

        return location;
    }


    private Map<USED_COLUMNS, Integer> colName2ColNr(Row headerLine) {

        final Map<USED_COLUMNS, Integer> colName2ColNr = new HashMap<>();
        for (Cell cell : headerLine) {

            colForName(cell.getStringCellValue())
                    .ifPresent(col -> colName2ColNr.put(col, cell.getColumnIndex()));
        }

        if( USED_COLUMNS.values().length != colName2ColNr.size()) {
            throw new RuntimeException("Some cols not found " + colName2ColNr.size());
        }
        return colName2ColNr;
    }

      private Optional<USED_COLUMNS> colForName(String name) {

        return Arrays.stream(USED_COLUMNS.values())
                .filter(col -> name.toLowerCase().startsWith(col.name().toLowerCase()))
                .findFirst();
    }

}
