package de.gartenbauvereineschwaben.tools;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.*;

import java.io.*;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.text.Collator;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.String.format;
import static java.util.Comparator.comparing;

public class StreuobstJsonExcel {

    private static final Logger LOG = Logger.getLogger(StreuobstJsonExcel.class.getName());
    private static final String SORTEN_SHEET_NAME = "Sorten";
    public static final String PASSWORD = "ogv";

    public static final String ROOT_PATH = "C:/Projects/rainer/ogvbezirkschwaben/bezirk.js/cdn/";

    public static Map<String,String> kreis2Kuerzel() {
        Map<String,String> kreis2Kuerzel =  new HashMap(Map.of(
                "LANDKREIS UND STADT AUGSBURG", "A"
                ,"LANDKREIS AICHACH-FRIEDBERG", "AIC"
                ,"LANDKREIS DILLINGEN", "DLG"
                ,"LANDKREIS DONAU-RIES", "DON"
                , "LANDKREIS GÜNZBURG", "GZ"
                , "LANDKREIS HEIDENHEIM", "HDH"
                ,"LANDKREIS KAUFBEUREN", "KF"
                , "LANDKREIS KEMPTEN","KE"
                ,"LANDKREIS LINDAU", "LI"));
        kreis2Kuerzel.put("LANDKREIS NEU-ULM", "NU");
        kreis2Kuerzel.put("LANDKREIS ULM", "UL");
        kreis2Kuerzel.put("LANDKREIS UNTERALLGÄU", "MN");
        kreis2Kuerzel.put("LANDKREIS OBERALLGÄU", "OA");
        kreis2Kuerzel.put("LANDKREIS OSTALLGÄU", "OAL");
        return kreis2Kuerzel;
    }
    private LocationObst[] readLocationsFromJson(String fileName) throws IOException {

        // create object mapper instance
        ObjectMapper mapper = new ObjectMapper();

        // convert a JSON string to a Book object
        LocationObst[] myObjects = mapper.readValue(Paths.get(fileName).toFile(), LocationObst[].class);

        return myObjects;
    }

    private void konsolidiereSorten(LocationObst[] baeume,  StreuobstSorte[] sorten, String filenameSortenJson) throws IOException {

//        Set<StreuobstSorte> sorten = Arrays.stream(gaerten)
//                .map(StreuobstSorte::new)
//                .collect(Collectors.toSet());

//        List<StreuobstSorte> sortenMitArt =  Arrays.stream(sorten)
//                .filter(sorte  -> sorte.getArt() != null && !sorte.getArt().isBlank())
//                .collect(Collectors.toList());

        final Map<String,StreuobstSorte> name2Sorte = Arrays.stream(sorten)
                .collect(Collectors.toMap(StreuobstSorte::getSortenName, sorte -> sorte));
        Arrays.stream(baeume)
                .filter(baum -> !name2Sorte.containsKey(baum.getSorte()))
                .forEach(baum -> {
                    LOG.warning("Baum ohne Sorte. Nr: " + baum.getNr() + " " + baum.getSorte() + " " );
                    name2Sorte.put(baum.getSorte(),new StreuobstSorte(baum.getSorte()));
                });
        writeSortenToJsonFile(new ArrayList<>(name2Sorte.values()),filenameSortenJson);

        // check unused sorten
        Arrays.stream(baeume)
                .forEach(baum -> name2Sorte.remove(baum.getSorte()));
        name2Sorte.values()
                .forEach(sorte -> LOG.warning("Sorte nicht verwendet Nr: " + sorte.getSortenName()));
    }

    private void writeToExcelFile(LocationObst[] baeume, StreuobstSorte[] sorten, String fileName) throws IOException {

        Collator usCollator = Collator.getInstance(Locale.GERMAN);
        usCollator.setStrength(Collator.TERTIARY);

        LOG.info(format("Write %s Gärten", baeume.length));

        Map<String,StreuobstSorte> name2Sorte = Arrays.stream(sorten)
                .collect(Collectors.toMap(StreuobstSorte::getSortenName, sorte -> sorte, (existing, replacement) -> replacement));
        Arrays.stream(baeume)
                .filter(baum -> !name2Sorte.containsKey(baum.getSorte()))
                .forEach(baum -> {
                    LOG.warning("Baum ohne Sorte " + baum.getNr() + " " + baum.getSorte() + " "  + baum.getArt());
                });

        try (XSSFWorkbook workbook = new XSSFWorkbook()) {

            Set<String> kreise = Arrays.stream(baeume)
                    .map(LocationObst::getKreis)
                    .collect(Collectors.toSet());

            kreise.stream()
                    .sorted()
                    .forEach(kreis -> {
                        XSSFSheet sheetKreis = workbook.createSheet(kreis);
                        addExcelHeaderRowBaum(sheetKreis);
                        Arrays.stream(baeume)
                                .filter(garten -> garten.getKreis().equals(kreis))
                                .forEach(garten -> addExcelRowBaum(sheetKreis, garten));
                        Stream.iterate(0, n -> n + 1)
                                .limit(10)
                                .forEach(sheetKreis::autoSizeColumn);
                    });

            XSSFCellStyle locked = workbook.createCellStyle();
            locked.setLocked(true);
            createTemplateSheetSorten(workbook, sorten, locked);

            FileOutputStream outputStream = new FileOutputStream(fileName);
            workbook.write(outputStream);
        }

    }

    private List<LocationObst> readExcelStreuobstTemplate(String fileName) throws IOException {

        Collection<String> kreiseKurzel = kreis2Kuerzel().values();


        Map<String, Sheet> sheetsByKreis = new HashMap<>();
        try (InputStream inp = new FileInputStream(fileName)) {

            Workbook workbook = new XSSFWorkbook(inp);
            Iterator<Sheet> sheetIterator = workbook.sheetIterator();
            while (sheetIterator.hasNext()) {
                Sheet sheet = sheetIterator.next();
                if(kreiseKurzel.contains(sheet.getSheetName().trim())) {
                    sheetsByKreis.put(sheet.getSheetName().trim(), sheet);
                }
            }
        }

        return sheetsByKreis.values().stream()
                .map(this::readSortenFromSheet)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    private List<LocationObst> treeLocationWithoutExistingSorte(StreuobstSorte[] bestehendeSorten, List<LocationObst> treeLocation) {

        Map<String,StreuobstSorte> sorteByName = Arrays.stream(bestehendeSorten)
                .collect(Collectors.toMap(StreuobstSorte::getSortenName, sorte -> sorte, (existing, replacement) -> replacement));

        return treeLocation.stream()
                .filter(tree -> !sorteByName.containsKey(tree.getSorte().trim()))
                .collect(Collectors.toList());

    }

    private List<StreuobstSorte> createSortenForTemplate(List<LocationObst> treesWithoutSorte) {

        Map<String, LocationObst> distinctSorten = treesWithoutSorte.stream()
                .collect(Collectors.toMap(LocationObst::getSorte, sorte -> sorte, (existing, replacement) -> existing));

        return distinctSorten.values().stream()
                .map(tree -> new StreuobstSorte(tree))
                .collect(Collectors.toList());
    }


    private  List<LocationObst> detectDoubleEntries( List<LocationObst> existingTrees,  List<LocationObst> newTrees) {

        var existingSorteByNr = existingTrees.stream()
                .collect(Collectors.toMap(LocationObst::getNr, tree -> tree));

        return newTrees.stream()
                .filter(newwTree -> existingSorteByNr.containsKey(newwTree.getNr()))
                .collect(Collectors.toList());
    }



    private List<LocationObst> readSortenFromSheet(Sheet sheet) {

        List<LocationObst> newTrees = new ArrayList<>();
        Iterator<Row> rowIter =  sheet.rowIterator();
        while (rowIter.hasNext()) {
            Row row = rowIter.next();
            if (row.getRowNum() >0 ) {
                LocationObst location = convertTemplateRow(row, sheet.getSheetName().trim());
                if (location != null) {
                    newTrees.add(location);
                }
            }
         }

        return newTrees;
    }

    private LocationObst convertTemplateRow(Row row, String kreisSheet) {

        String treeNr = getCellValueString(row.getCell(0));
        String sorte = getCellValueString(row.getCell(1));
        String art = getCellValueString(row.getCell(2));
        String gemeinde = getCellValueString(row.getCell(3));
        String kreis = getCellValueString(row.getCell(4));
        Double lng = getCellValueDouble(row.getCell(5));
        Double lat = getCellValueDouble(row.getCell(6));

        if(lng == null || lat == null) {
            if (sorte != null) {
                LOG.warning(format("Region: '%s' Row: %d - Invalid Lat/long for tree '%s' ", kreisSheet,row.getRowNum(), treeNr));
            }
            return null;
        } else if(!kreisSheet.equals(kreis)) {
            LOG.warning(format("Region: '%s' Row: %d - Invalid region '%s' in line  ", kreisSheet,row.getRowNum(), kreis));
            return null;
        } else {
            LocationObst obst = new LocationObst();
            obst.setArt(art);
            obst.setGemeinde(gemeinde);
            obst.setNr(treeNr);
            obst.setSorte(sorte);
            obst.setKreis(kreis);
            obst.setOrt(new LocationObst.Place(lat,lng ));

            return obst;
        }
    }

    private String getCellValueString(Cell cell) {

        if(cell == null || cell.getCellType() == CellType.BLANK) {
            return null;
        } else if (cell.getCellType() == CellType.NUMERIC) {
            return String.valueOf(cell.getNumericCellValue());
        } else {
            return  cell.getStringCellValue();
        }
    }

    private Double getCellValueDouble(Cell cell) {

        if(cell == null || cell.getCellType() == CellType.BLANK) {
            return null;
        } else if (cell.getCellType() == CellType.STRING)
            try {
                return  Double.parseDouble(cell.getStringCellValue());
            } catch(NumberFormatException ex) {
                return null;
            }
        else {
            return  cell.getNumericCellValue();
        }
    }
   private void writeToExcelStreuobstTemplate( StreuobstSorte[] sorten, String fileName) throws IOException {

        try (XSSFWorkbook workbook = new XSSFWorkbook()) {


            XSSFCellStyle locked = workbook.createCellStyle();
            locked.setLocked(true);
            XSSFCellStyle unlocked = workbook.createCellStyle();
            unlocked.setLocked(false);
            XSSFCellStyle decimalStyle = workbook.createCellStyle();
            decimalStyle.setDataFormat(workbook.createDataFormat().getFormat("#0.###########"));
            decimalStyle.setLocked(false);
            createTemplateSheetSorten(workbook, sorten, locked);

            final String SORTEN_DROPDOWN = "SortenDrop";
            final String SORTEN_SHEET = "Sorten";
            createNamedRange(workbook, SORTEN_SHEET, SORTEN_DROPDOWN, 0, 2, sorten.length+1);

            Collection<String> kreise = kreis2Kuerzel().values();

            kreise.stream()
                    .sorted()
                    .forEach(kreis -> {
                        XSSFSheet sheetKreis = workbook.createSheet(kreis);
                        addExcelHeaderRowBaumTemplate(sheetKreis);
                        Stream.iterate(0, i -> i+1).limit(100)
                                .forEach(x -> addExcelRowTemplateBaum(sheetKreis, x, kreis, SORTEN_SHEET, SORTEN_DROPDOWN,2,sorten.length+1
                                , locked,unlocked,decimalStyle));

                    });


            FileOutputStream outputStream = new FileOutputStream(fileName);
            workbook.write(outputStream);
        }
    }

    private void createTemplateSheetSorten(XSSFWorkbook workbook, StreuobstSorte[] sorten, XSSFCellStyle locked) {

        XSSFSheet sheetSorten = workbook.createSheet(SORTEN_SHEET_NAME);
        addExcelHeaderSorte(sheetSorten, locked);
        Arrays.stream(sorten)
                .sorted( comparing(StreuobstSorte::getSortenNameCollation).thenComparing(StreuobstSorte::getArt))
                .forEach(sorte -> addExcelRowSorte(sheetSorten, sorte));
        Stream.iterate(0, n -> n + 1)
                .limit(10)
                .forEach(sheetSorten::autoSizeColumn);
        sheetSorten.protectSheet(PASSWORD);
        sheetSorten.lockFormatCells(true);
        sheetSorten.lockFormatColumns(true );
    }

    private void addExcelHeaderSorte(XSSFSheet sheet, XSSFCellStyle locked) {

        // https://www.streuobst-in-bayern.de/listing/type=AppleType
        // Aargauer Jubiläumsapfel
        // https://www.streuobst-in-bayern.de/we2p/api/?controller=search&action=completion&query=Brettacher&filters=[{%22type%22:%22type%22,%22value%22:%22AppleType%22,%22temporary%22:false}]
        int cellnum = 0;
        Row row = sheet.createRow(sheet.getLastRowNum()+1);
        Cell cell = row.createCell(cellnum++);
        cell.setCellValue("Sorten Name");
        cell.setCellStyle(locked);
        cell = row.createCell(cellnum++);
        cell.setCellValue("Art");
        cell.setCellStyle(locked);
        cell = row.createCell(cellnum++);
        cell.setCellValue("Id (Streuobst in Bayern)");
        cell.setCellStyle(locked);
        cell = row.createCell(cellnum++);
        cell.setCellValue("Link");
        cell.setCellStyle(locked);
    }

    private void addExcelRowSorte(XSSFSheet sheet, StreuobstSorte sorte) {

        // https://www.streuobst-in-bayern.de/listing/type=AppleType
        // Aargauer Jubiläumsapfel
        // https://www.streuobst-in-bayern.de/we2p/api/?controller=search&action=completion&query=Brettacher&filters=[{%22type%22:%22type%22,%22value%22:%22AppleType%22,%22temporary%22:false}]
        int cellnum = 0;
        Row row = sheet.createRow(sheet.getLastRowNum()+1);
        Cell cell = row.createCell(cellnum++);
        cell.setCellValue(sorte.getSortenName());
        cell = row.createCell(cellnum++);
        cell.setCellValue(sorte.getArt());
        cell = row.createCell(cellnum++);
        cell.setCellValue(sorte.getLinkId());
        cell = row.createCell(cellnum++);
        if (sorte.getLinkId() != null) {
            cell.setCellFormula("HYPERLINK(\"https://www.streuobst-in-bayern.de/detail/id=" +sorte.getLinkId() +"\")");
        }
    }

    private Row addExcelHeaderRowBaum(XSSFSheet sheet) {

        int cellnum = 0;
        Row row = sheet.createRow(sheet.getLastRowNum()+1);
        row.createCell(cellnum++).setCellValue("Nr");
        row.createCell(cellnum++).setCellValue("Sorte");
        row.createCell(cellnum++).setCellValue("Art");
        row.createCell(cellnum++).setCellValue("Gemeinde");
        row.createCell(cellnum++).setCellValue("Kreis");
        row.createCell(cellnum++).setCellValue("GPS longitude");
        row.createCell(cellnum).setCellValue("GPS latitude");
        return row;
    }

    private Row addExcelHeaderRowBaumTemplate(XSSFSheet sheet) {

        Row row = addExcelHeaderRowBaum(sheet);
        int cellnum = row.getLastCellNum();
        row.createCell(cellnum++).setCellValue("Neue Sorte");
        sheet.setColumnWidth(cellnum-1, 20 * 256);
        row.createCell(cellnum).setCellValue("Art neue Sorte");
        sheet.setColumnWidth(cellnum, 20 * 256);
        return row;
    }

    private void addExcelRowTemplateBaum(XSSFSheet sheet, int rowNr, String kreis, String sortenSheet, String sortenDropDown
            , int startRow, int endRow, XSSFCellStyle locked, XSSFCellStyle unlocked, XSSFCellStyle decimalStyle) {

        int cellnum = 0;
        Row row = sheet.createRow(sheet.getLastRowNum()+1);
        Cell cell = row.createCell(cellnum++);
        cell.setCellValue(kreis + String.format("%04d", (rowNr+1)));
        cell.setCellStyle(unlocked);
        sheet.setColumnWidth(cellnum-1, 256*10);

        cell = row.createCell(cellnum++);
        cell.setCellStyle(unlocked);
        createDropdownList(row, 1, sortenDropDown);
        sheet.setColumnWidth(cellnum-1, 50 * 256);


        cell = row.createCell(cellnum++);
        String formulaSheetName = "'" + sortenSheet + "'";
        String startCell = cell(0, startRow);
        String endCell = cell(3, endRow);
        String range = formulaSheetName + "!" + startCell + ":" + endCell;
        cell.setCellFormula("IFERROR(VLOOKUP("+ cell(1,row.getRowNum()+1) + "," + range + ",2,TRUE),\"-\")");

        sheet.setColumnWidth(cellnum-1, 10 * 256);

        cell = row.createCell(cellnum++);
        cell.setCellValue(""); // Gemeinde
        cell.setCellStyle(unlocked);
        sheet.setColumnWidth(cellnum-1, 20 * 256);
        cell = row.createCell(cellnum++);
        cell.setCellValue(kreis); // Kreis
        sheet.setColumnWidth(cellnum-1, 10 * 256);
        cell = row.createCell(cellnum++);
        cell.setCellValue(""); // GPS long
        cell.setCellStyle(decimalStyle);
        sheet.setColumnWidth(cellnum-1, 20 * 256);
        cell = row.createCell(cellnum++);
        cell.setCellValue(""); // GPS lat
        cell.setCellStyle(decimalStyle);
        sheet.setColumnWidth(cellnum-1, 20 * 256);
    }

    public void createNamedRange(XSSFWorkbook workbook, String sheetName, String dropDownName, int col, int startRow, int endRow) {

        Name name = workbook.createName();
        name.setNameName(dropDownName);
        String formulaSheetName = "'" + sheetName + "'";
        String startCell = cell(col, startRow);
        String endCell = cell(col, endRow);
        name.setRefersToFormula(formulaSheetName + "!" + startCell + ":" + endCell);
    }

    private static String cell(int col, int row) {

        char colIndex = (char) ('A' + col);
        return "$" + colIndex + "$" + row;
    }

    private void createDropdownList(Row newRow, int colNr, String rangeName) {

        XSSFDataValidationHelper dvHelper = new XSSFDataValidationHelper((XSSFSheet) newRow.getSheet());
        XSSFDataValidationConstraint dvConstraint = (XSSFDataValidationConstraint) dvHelper.createFormulaListConstraint(rangeName);
        CellRangeAddressList addressList = new CellRangeAddressList(newRow.getRowNum(), newRow.getRowNum(), colNr, colNr);
        XSSFDataValidation validation = (XSSFDataValidation) dvHelper.createValidation(dvConstraint, addressList);
        validation.setSuppressDropDownArrow(true);
        validation.setShowErrorBox(true);
        newRow.getSheet().addValidationData(validation);
    }


    private void addExcelRowBaum(XSSFSheet sheet, LocationObst streuobstBaum) {

        int cellnum = 0;
        Row row = sheet.createRow(sheet.getLastRowNum()+1);
        Cell cell = row.createCell(cellnum++);
        cell.setCellValue(streuobstBaum.getNr());
        cell = row.createCell(cellnum++);
        cell.setCellValue(streuobstBaum.getSorte());
        cell = row.createCell(cellnum++);
        cell.setCellValue(streuobstBaum.getArt());
        cell = row.createCell(cellnum++);
        cell.setCellValue(streuobstBaum.getGemeinde());
        cell = row.createCell(cellnum++);
        cell.setCellValue(streuobstBaum.getKreis());
        cell = row.createCell(cellnum++);
        cell.setCellValue(streuobstBaum.getOrt().getLng());
        cell = row.createCell(cellnum++);
        cell.setCellValue(streuobstBaum.getOrt().getLt());
    }

    private static void writeSortenToJsonFile(List<StreuobstSorte> locations, String fileName) throws IOException {

        File targetFile = new File(fileName);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writeValue(targetFile, locations);
    }

    private void writeBaeumeToJsonFile(List<LocationObst> baeume, String fileName) throws IOException {

        File targetFile = new File(fileName);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writeValue(targetFile, baeume);
    }

    private static StreuobstSorte[] readSortenFromJson(String fileName) throws IOException {

        // create object mapper instance
        ObjectMapper mapper = new ObjectMapper();

        // convert a JSON string to a Book object
        StreuobstSorte[] myObjects = mapper.readValue(Paths.get(fileName).toFile(), StreuobstSorte[].class);

        return myObjects;
    }

    public void setUrlInSorte(StreuobstSorte sorte) {
        sorte.setLinkId(getSortenIdBeiSortenname(sorte.getSortenName()));
    }

    public String getSortenIdBeiSortenname(String sortenName) {
        try {
            // https://www.streuobst-in-bayern.de/we2p/api/?controller=search&action=completion&query=Brettacher&filters=[{%22type%22:%22type%22,%22value%22:%22AppleType%22,%22temporary%22:false}]
            //https://www.streuobst-in-bayern.de/detail/id=628e22c14aa7e13f743a6115

            String surl = "https://www.streuobst-in-bayern.de/we2p/api/?controller=search&action=completion&query="+ URLEncoder.encode(sortenName, StandardCharsets.UTF_8)
                    +"&filters=[{%22type%22:%22type%22,%22value%22:%22AppleType%22,%22temporary%22:false}";


            URL url = new URL(surl);
            InputStream is = url.openConnection().getInputStream();

            BufferedReader streamReader = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
            StringBuilder responseStrBuilder = new StringBuilder();

            String inputStr;
            while ((inputStr = streamReader.readLine()) != null)
                responseStrBuilder.append(inputStr);

            ObjectMapper mapper = new ObjectMapper();
            JsonNode responseJsonNode = mapper.readTree(responseStrBuilder.toString());

            JsonNode node = responseJsonNode.at("/hits/hits/0/_source/detailLink");
            String linkId = (node.isMissingNode()) ? null :  node.asText();
            String detailId = (linkId != null && linkId.startsWith("/detail/id=")) ? linkId.substring("/detail/id=".length()) : null;
            LOG.info(sortenName + " " + detailId);
            return detailId;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void generateExcelTemplateZurEingabeDerBaeume() throws IOException {

        StreuobstJsonExcel json2Excel = new StreuobstJsonExcel();
        StreuobstSorte[] sorten = json2Excel.readSortenFromJson(ROOT_PATH + "Sorten_2023_10.json");
        json2Excel.writeToExcelStreuobstTemplate( sorten, ROOT_PATH + "StreuobstHomepageTemplate.xlsx");

    }

    public static void readExcelTemplateZurEingabeDerBaeume() throws IOException {

        StreuobstJsonExcel json2Excel = new StreuobstJsonExcel();
        List<LocationObst> allNewTrees = json2Excel.readExcelStreuobstTemplate("C:\\Project\\rainer\\ogvbezirkschwaben\\bezirk.js\\cdn\\StreuobstHomepageTemplate.xlsx");
        LocationObst[] existingTrees = json2Excel.readLocationsFromJson("C:\\Project\\rainer\\ogvbezirkschwaben\\bezirk.js\\cdn\\StreuobstHomepage.json");
        StreuobstSorte[] bestehendeSorten = json2Excel.readSortenFromJson("C:\\Project\\rainer\\ogvbezirkschwaben\\bezirk.js\\cdn\\Sorten_konsolidiert.json");

        List<LocationObst> treesWithoutSorte = json2Excel.treeLocationWithoutExistingSorte(bestehendeSorten, allNewTrees);
        List<StreuobstSorte> neueSorten = json2Excel.createSortenForTemplate(treesWithoutSorte);

        List<LocationObst>  doubleEntries = json2Excel.detectDoubleEntries(Arrays.asList(existingTrees), allNewTrees);
        if (doubleEntries.size() == 0) {
            allNewTrees.addAll(Arrays.stream(existingTrees).toList());
            json2Excel.writeBaeumeToJsonFile(allNewTrees, "C:\\Project\\rainer\\ogvbezirkschwaben\\bezirk.js\\cdn\\StreuobstHomepage_2023_07.json");
        }

    }



    public static void generateJsonMitAllenSortenKonsolidiert() throws IOException {

        StreuobstJsonExcel json2Excel = new StreuobstJsonExcel();
        StreuobstSorte[] sortenAktuell = json2Excel.readSortenFromJson(ROOT_PATH + "Sorten_2023_10.json");
        LocationObst[] locations = json2Excel.readLocationsFromJson(ROOT_PATH + "StreuobstHomepage_2023_07.json");
        json2Excel.konsolidiereSorten(locations, sortenAktuell,ROOT_PATH + "Sorten_2023_10.json");
    }

    public static void generateExcelMitAllenBaeumen() throws IOException {

        StreuobstJsonExcel json2Excel = new StreuobstJsonExcel();
        LocationObst[] locations = json2Excel.readLocationsFromJson(ROOT_PATH + "StreuobstHomepage_2023_07.json");
        StreuobstSorte[] sorten = json2Excel.readSortenFromJson(ROOT_PATH + "Sorten_2023_10.json");

        json2Excel.writeToExcelFile(locations, sorten, ROOT_PATH + "StreuobstHomepage_2023_10.xlsx");
    }

    public static void generateJsonMitAllenBaeumenKonsolidiert() throws IOException {

        StreuobstJsonExcel json2Excel = new StreuobstJsonExcel();
        LocationObst[] locations = json2Excel.readLocationsFromJson("C:\\Project\\rainer\\ogvbezirkschwaben\\bezirk.js\\cdn\\StreuobstHomepage.json");
        StreuobstSorte[] sorten = json2Excel.readSortenFromJson("C:\\Project\\rainer\\ogvbezirkschwaben\\bezirk.js\\cdn\\Sorten_konsolidiert.json");

        json2Excel.writeBaeumeToJsonFile(Arrays.stream(locations).toList(), "C:\\Project\\rainer\\ogvbezirkschwaben\\bezirk.js\\cdn\\AlleBaeumeBezirkSchwaben.json");
    }


    public static void main(String[] args) throws IOException {

        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out), true, StandardCharsets.UTF_8));
        //LocationGarten[] locations = readLocationsFromJson("C:/Project/rainer/ogvbezirkschwaben/bezirk.java/src/main/resources/de/gartenbauvereineschwaben/tools/GartenTuer2023.json");
        //writeGaertenToExcelFile(locations, "C:/Project/rainer/ogvbezirkschwaben/bezirk.java/src/main/resources/de/gartenbauvereineschwaben/tools/GartenTuer2023_.xlsx");

        generateExcelMitAllenBaeumen();
        generateExcelTemplateZurEingabeDerBaeume();

//        Arrays.stream(sorten)
//                .filter(sorte -> sorte.getArt().equals("Apfel"))
//                .forEach(sorte -> json2Excel.setUrlInSorte(sorte));
//        writeSortenToJsonFile(Arrays.stream(sorten).toList(),"C:\\Project\\rainer\\ogvbezirkschwaben\\bezirk.js\\cdn\\SortenWithLink.json");

    }

    private static void findeBaumeMitFalscherNummer() throws IOException {
        List<LocationObst> baeume = List.of(new StreuobstJsonExcel().readLocationsFromJson("C:\\Projects\\rainer\\ogvbezirkschwaben\\bezirk.js\\cdn\\StreuobstHomepage_2023_07.json"));

        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out), true, StandardCharsets.UTF_8));
        var sortenMitNummer = baeume.stream()
                .filter(StreuobstJsonExcel::hasNumber)
                .toList();
        sortenMitNummer.forEach(sorte -> System.out.printf("Nr: %s, Sorte %s, Ort: %s \n", sorte.getNr(), sorte.getSorte(), sorte.getGemeinde()));
    }

    private static boolean hasNumber(LocationObst sorte) {
        try {
            String number = sorte.getSorte().replaceAll("[^0-9]", "");
            return (!number.isEmpty() && Integer.parseInt(number) != (int) Double.parseDouble(sorte.getNr()));
        } catch(Throwable ex) {
            return false;
        }

    }
}
