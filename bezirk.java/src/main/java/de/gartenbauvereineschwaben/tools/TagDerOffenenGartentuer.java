package de.gartenbauvereineschwaben.tools;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Stream;

public class TagDerOffenenGartentuer {

    private static final Logger LOG = Logger.getLogger(TagDerOffenenGartentuer.class.getName());

    public static Map<String,String> kreis2Kuerzel() {
        return Map.of("LANDKREIS OBERALLGÄU UND STADT KEMPTEN","OA"
                , "LANDKREIS UNTERALLGÄU", "MN"
                ,"LANDKREIS AICHACH-FRIEDBERG", "AIC"
                ,"LANDKREIS DONAU-RIES", "DON"
                ,"LANDKREIS DILLINGEN", "DLG"
                ,"LANDKREIS UND STADT AUGSBURG", "A"
                ,"LANDKREIS LINDAU", "LI"
                ,"LANDKREIS OSTALLGÄU", "OAL"
                , "LANDKREIS GÜNZBURG", "GZ"
                , "LANDKREIS NEU-ULM", "NU"
        );
    }

    public static JsonNode getLatitudeLongitudeByAddress(String completeAddress) {
        try {
            //https://nominatim.openstreetmap.org/search?q=Kirchplatz+8+Roggenburg&format=json&polygon=1&addressdetails=1

//            String surl = "https://maps.googleapis.com/maps/api/geocode/json?address="+ URLEncoder.encode(completeAddress, "UTF-8")+"&key=";

            String surl = "https://nominatim.openstreetmap.org/search?q="+ URLEncoder.encode(completeAddress, "UTF-8")
                    +"&format=json&polygon=1&addressdetails=1";


            URL url = new URL(surl);
            InputStream is = url.openConnection().getInputStream();

            BufferedReader streamReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            StringBuilder responseStrBuilder = new StringBuilder();

            String inputStr;
            while ((inputStr = streamReader.readLine()) != null)
                responseStrBuilder.append(inputStr);

            ObjectMapper mapper = new ObjectMapper();
            JsonNode responseJsonNode = mapper.readTree(responseStrBuilder.toString());

            JsonNode node = responseJsonNode.get(0);
            return node;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static Sheet readExcel(String fileName) throws IOException {

        try (InputStream inp = TagDerOffenenGartentuer.class.getResourceAsStream(fileName)) {

            Workbook workbook = new XSSFWorkbook(inp);

            return workbook.getSheetAt(0);
        }
    }

    private static LocationGarten[] readLocationsFromJson(String fileName) throws IOException {

        // create object mapper instance
        ObjectMapper mapper = new ObjectMapper();
        System.out.println(Paths.get(fileName).toFile().getAbsolutePath());
        // convert a JSON string to a Book object
        LocationGarten[] myObjects = mapper.readValue(Paths.get(fileName).toFile(), LocationGarten[].class);

        // print book
        System.out.println(myObjects);

        return myObjects;
    }

    public static void main(String[] args) throws IOException {

        //LocationGarten[] locations = readLocationsFromJson("C:/Project/rainer/ogvbezirkschwaben/bezirk.java/src/main/resources/de/gartenbauvereineschwaben/tools/GartenTuer2023.json");
        //writeGaertenToExcelFile(locations, "C:/Project/rainer/ogvbezirkschwaben/bezirk.java/src/main/resources/de/gartenbauvereineschwaben/tools/GartenTuer2023_.xlsx");

        List<LocationGarten> locations = readLocationsGartenFromExcel("GartenTuer2023.xlsx");
        writeLocationsToJsonFile(locations, "C:/Project/rainer/ogvbezirkschwaben/bezirk.java/src/main/resources/de/gartenbauvereineschwaben/tools/GartenTuer2023_.json");
    }

    private static void writeLocationsToJsonFile(List<LocationGarten> locations, String fileName) throws IOException {

        File targetFile = new File(fileName);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writeValue(targetFile, locations);
    }

    private static void writeGaertenToExcelFile(LocationGarten[] gaerten, String fileName) throws IOException {

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Gärten");
        addExcelHeaderRow(sheet);
        Arrays.stream(gaerten).forEach( garten -> addExcelRow(sheet, garten));
        Stream.iterate(0, n -> n + 1)
                .limit(10)
                .forEach(x ->sheet.autoSizeColumn(x));


        FileOutputStream outputStream = new FileOutputStream(fileName);
        workbook.write(outputStream);

    }

    private  static void addExcelHeaderRow(XSSFSheet sheet) {

        int cellnum = 0;
        Row row = sheet.createRow(sheet.getLastRowNum()+1);
        Cell cell = row.createCell(cellnum++);
        cell.setCellValue("Name");
        cell = row.createCell(cellnum++);
        cell.setCellValue("Straße");
        cell = row.createCell(cellnum++);
        cell.setCellValue("Ort");
        cell = row.createCell(cellnum++);
        cell.setCellValue("Ortsteil");
        cell = row.createCell(cellnum++);
        cell.setCellValue("Text");
        cell = row.createCell(cellnum++);
        cell.setCellValue("Kreis");
        cell = row.createCell(cellnum++);
        cell.setCellValue("GPS longitude");
        cell = row.createCell(cellnum);
        cell.setCellValue("GPS latitude");
    }

    private  static void addExcelRow(XSSFSheet sheet, LocationGarten garten) {

        int cellnum = 0;
        Row row = sheet.createRow(sheet.getLastRowNum()+1);
        Cell cell = row.createCell(cellnum++);
        cell.setCellValue(garten.getName());
        cell = row.createCell(cellnum++);
        cell.setCellValue(garten.getStrasse());
        cell = row.createCell(cellnum++);
        cell.setCellValue(garten.getOrt());
        cell = row.createCell(cellnum++);
        cell.setCellValue(garten.getOrtsteil());
        cell = row.createCell(cellnum++);
        cell.setCellValue(garten.getText());
        cell = row.createCell(cellnum++);
        cell.setCellValue(garten.getKreis());
        cell = row.createCell(cellnum++);
        cell.setCellValue(garten.getGps().lng);
        cell = row.createCell(cellnum++);
        cell.setCellValue(garten.getGps().lt);
    }

    private static List<LocationGarten> readLocationsGartenFromExcel(String excelFileName) throws IOException {

        Sheet sheet = readExcel(excelFileName);

        List<LocationGarten> locations = new ArrayList<>();
        for (Row row : sheet) {
            if(row.getRowNum() != 0) { // ignore header

                if (row.getCell(0) == null) {
                    LOG.info("Skip row " + row.getRowNum());
                    continue;
                }
                LocationGarten garten = gexcelRowToLocationGarten(row);
                locations.add(garten);
            }

        }
        return locations;
    }

    private static boolean cellEmpty(Row row, int nr) {
        Cell c = row.getCell(nr);
        return c == null || c.getCellType() == CellType.BLANK;
    }

    private static LocationGarten gexcelRowToLocationGarten(Row row) {

        String name = row.getCell(0).getStringCellValue();
        String street = row.getCell(1).getStringCellValue();
        String town = row.getCell(2).getStringCellValue();
        String townExt = cellEmpty(row, 3) ? "" : row.getCell(3).getStringCellValue();
        String text = row.getCell(4).getStringCellValue();
        String kreis = row.getCell(5).getStringCellValue();
        Double longitude = cellEmpty(row, 6) ? null : row.getCell(6).getNumericCellValue();
        Double latitude = cellEmpty(row, 7) ? null : row.getCell(7).getNumericCellValue();


        LOG.info(name);
        LocationGarten garten = new LocationGarten();
        if (latitude != null && longitude != null) {
            garten.getGps().setLng(longitude);
            garten.getGps().setLt(latitude);

        } else {
            JsonNode gps = getLatitudeLongitudeByAddress(street + "," + town);
            if (gps != null) {
                double lon = gps.get("lon").asDouble();
                double lat = gps.get("lat").asDouble();
                garten.getGps().setLng(lon);
                garten.getGps().setLt(lat);
            }
        }

        garten.setName(name);
        garten.setStrasse(street);
        garten.setOrt(town);
        garten.setOrtsteil(townExt);
        garten.setKreis((kreis2Kuerzel().get(kreis) == null) ? kreis : kreis2Kuerzel().get(kreis));
        garten.setText(text);
        return garten;
    }
}
