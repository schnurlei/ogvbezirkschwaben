import { createApp } from 'vue'
import { Quasar } from 'quasar'
import './style.css'
import "vue3-openlayers/dist/vue3-openlayers.css";
import '@quasar/extras/material-icons/material-icons.css'
import 'quasar/src/css/index.sass'
import App from './App.vue'
import OpenLayersMap from "vue3-openlayers";

const myApp = createApp(App);

myApp.use(OpenLayersMap,Quasar)
myApp.use(Quasar, {
    plugins: {}, // import Quasar plugins and add here
})
myApp.mount('#app')
